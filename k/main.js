let canvas = document.getElementById("can");
let context = canvas.getContext("2d");
let W = window.innerWidth;
let H = window.innerHeight;
let ratio = window.devicePixelRatio;
canvas.width = W * ratio;
canvas.height = H * ratio;
canvas.style.width = W + "px";
canvas.style.height = H + "px";
context.scale(ratio, ratio);
context.font = "25px cursive";
context.textBaseline = "middle"


document.addEventListener("keydown", bas);
document.addEventListener("keyup", haut);

function LESSGO() {
    document.getElementById("menu").style.display = "none";
    document.getElementById("game").style.display = "block";
    mainloop();
}

const imavion = document.getElementById('avion');
const imnuage = document.getElementById('nuag');
const affScore = document.getElementById('score');

let ecart = 100;

const goods = [new Image(), new Image(), new Image(), new Image()];
goods[0].src = "r.png";
goods[1].src = "uol.png";
goods[2].src = "a.png";
goods[3].src = "polé.png";
const badwords = ["Z", "EG", "IR", "COVID", "LR"]
class Dieu {
    constructor(x, y, img, points) {
        this.x = x;
        this.y = y;
        this.img = img;
        this.w = 50;
        this.h = 50;
        this.points = points;
    }
    draw() {
        context.drawImage(this.img, this.x - this.w / 2, this.y - this.h / 2, this.w, this.h);
    }
    move() {
        this.y += 1;
        if (!(this.y - this.h / 2 > blat.y + blat.h / 2 ||
                this.x - this.w / 2 > blat.x + blat.w / 2 ||
                this.y + this.h / 2 < blat.y - blat.h / 2 ||
                this.x + this.w / 2 < blat.x - blat.w / 2)) {

            score += this.points;
            for (let i = 0; i < 10; i++) {
                ges.push(new co2(this.x, this.y, i / 5 * Math.PI, this.points > 0 ? "lightgreen" : "coral"));
            }
            this.y = H + 1;
        }

    }
}
class Demonds extends Dieu {
    constructor(x, y, img, points, bad) {
        super(x, y, img, points);
        this.w = 100;
        this.h = 50
        this.badword = bad;
        this.taille = context.measureText(this.badword)
    }
    draw() {
        context.drawImage(this.img, this.x - this.w / 2, this.y - this.h / 2, this.w, this.h);
        context.fillStyle = "red";
        context.fillText(this.badword, this.x - this.taille.width / 2, this.y);
    }
}

class Avion {
    constructor(x, y, depx, depy, fakex, fakey, w, h) {
        this.x = x;
        this.y = y;
        this.dirx = depx;
        this.diry = depy;
        this.fakex = fakex;
        this.fakey = fakey;
        this.w = w;
        this.h = h;
    }
    draw() {
        context.drawImage(imavion, this.x - this.w / 2, this.y - this.h / 2, this.w, this.h);
    }
    move() {
        this.fakex += this.dirx * 10;
        this.fakey += this.diry * 10;
        this.x += (this.fakex - this.x) / 8;
        this.y += (this.fakey - this.y) / 8;
        if (this.diry != 1 && Math.random() > .5) {
            ges.push(new co2(this.x + this.w / 4, this.y, Math.PI / 2 + Math.random() * Math.PI / 8 - Math.PI / 16, "lightblue"));
            ges.push(new co2(this.x - this.w / 4, this.y, Math.PI / 2 + Math.random() * Math.PI / 8 - Math.PI / 16, "lightblue"));
        }
    }
}

class co2 {
    constructor(x, y, ang, color) {
        this.x = x
        this.y = y
        this.angle = ang;
        this.forse = Math.random() * 4 + 4;
        this.vx = Math.cos(this.angle) * this.forse;
        this.vy = Math.sin(this.angle) * this.forse;
        this.color = color;
        this.alpha = 1;
    }
    update() {
        this.alpha -= 1 / 100;
        if (this.alpha <= 0) {
            this.aaron = true;
        }
        this.x += this.vx;
        this.y += this.vy;
    }

    draw() {
        context.fillStyle = this.color;
        context.globalAlpha = this.alpha;
        context.fillRect(this.x, this.y, 5, 10);
        context.globalAlpha = 1;
    }
}

class NY extends Dieu {
    constructor() {
        super(W / 2, H / 2, imnuage, 1);
        this.x = W / 2;
        this.y = 0;
        context.font = "100px serif";
        this.dim = context.measureText("🗽");
        this.w = this.dim.width;
        this.h = this.dim.height;
    }
    draw() {
        context.fillText("🗽", this.x, this.y);
    }
}
let tick = 0;
let volants = [];
let ges = [];
let score = 0;
let vie = 3;

function haut(e) {
    if (e.keyCode == 37 || e.keyCode == 39) {
        blat.dirx = 0;
    }
    if (e.keyCode == 38 || e.keyCode == 40) {
        blat.diry = 0;
    }
}

function bas(e) {
    if (e.keyCode == 38) {
        blat.diry = -1;
    }
    if (e.keyCode == 40) {
        blat.diry = 1;
    }
    if (e.keyCode == 37) {
        blat.dirx = -1;
    }
    if (e.keyCode == 39) {
        blat.dirx = 1;
    }
}

blat = new Avion(W / 2, H / 2, 0, 0, W / 2, H / 2, 50, 50);

function mainloop() {
    context.fillStyle = "#6174ee";
    context.fillRect(0, 0, W, H);

    tick++;
    if (tick % ecart == 0) {
        volants.push(new Dieu(Math.random() * W,
            0,
            goods[parseInt(Math.random() * 4)], 1));
        volants.push(new Demonds(Math.random() * W,
            0,
            imnuage, -1, badwords[parseInt(Math.random() * badwords.length)]));

    }
    if (score == 50) {
        volants = [];
        ecart = 0;
        volants.push(new NY());
        affScore.style.display = "none";
        score = 51;
    }
    if (score == 52) {
        canvas.style.display = "none";
        location.replace("https://www.youtube.com/watch?v=oMX1sc3eOTE");
    }

    for (let vol of volants) {
        vol.draw();
        vol.move();
    }
    for (gaz of ges) {
        gaz.draw()
        gaz.update()
    }
    volants = volants.filter((vol) => vol.y < H);
    ges = ges.filter((n) => !n.aaron);
    blat.draw()
    blat.move()
    affScore.innerHTML = score;

    requestAnimationFrame(mainloop);
}